import networkx as nx
from collections import Counter
from globaltesting_task import is_href_internal


def avg_links_to_external(G):
    cnt_internal = len([1 for node in G.nodes if is_href_internal(node)])
    cnt_external = len(G.nodes) - cnt_internal
    return cnt_external/cnt_internal


def avg_num_internal_links(G):
    cnt_internal = len([1 for node in G.nodes if is_href_internal(node)])
    cnt_to_internal = len([1 for edge in G.edges if is_href_internal(edge[1])])
    return cnt_to_internal/cnt_internal


def destination_counter(G):
    destinations = [edge[1] for edge in G.edges]
    return Counter(destinations)


def page_size(G, page_url):
    for node in G.nodes(data=True):
        if node[0] == page_url:
            return node[1]['length_of_html']
    return 'Page not found in the map of the website'


def main():
    # reading graph from file
    G = nx.read_gpickle("website_map.gpickle")

    # calculating diamater of the graph - the max distance between nodes
    # diameter = nx.algorithms.distance_measures.diameter(G)
    # print(f'Diameter of the graph: {diameter}')

    print(f'average number of links to different websites: {avg_links_to_external(G)}\n')

    print(f'average number of links to internal pages: {avg_num_internal_links(G)}\n')

    # how often each website was a destination - we can get most and least frequent destinations
    dest_cnt = destination_counter(G)
    print(f'Three most common destinations: {dest_cnt.most_common(3)}\n')
    print(f'Three least common destinations: {dest_cnt.most_common()[:-3-1:-1]}\n')
    
    # shortest path between two nodes
    print('Shortest path between two nodes: ', nx.shortest_path(G, source='https://www.globalapptesting.com', 
    target='https://www.globalapptesting.com/blog/remote-working-has-changed-software-development-forever.-heres-why?hs_amp=true'), '\n')

    print('Shortest path between two nodes: ', nx.shortest_path(G, source='https://www.globalapptesting.com', 
    target='https://www.globalapptesting.com/security'), '\n')

    # length of html for given page
    page_url = 'https://www.globalapptesting.com/blog/author/kate-passby/page/0'
    print(f'Size of {page_url}: {page_size(G, page_url)}')

if __name__ == '__main__':
    main()
