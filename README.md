# README #

### What is this repository for? ###

* This repo is for global testing recruitment task

### How do I get set up? ###

* I managed to get it working using libraries listed in requirements.txt
* There are two scripts - globaltesting_task.py creates a map of the website and saves it to gpickle file. It also does very basic plot of the map and saves it to png file. Second file is graph_analysis.py - in this one I load the saved graph and display some statistics about it.

I have chosen the graph representation for the website map. I recursively searched through the link on the webpage, creating graph on the fly. Later plotted and saved the graph.

### Requirements for the project ###
* We want to start from the homepage (https://globalapptesting.com) and find any possible transitions between subpages. You may focus only on using links ( tags). - done 

* We are interested in the best possible map representation: could you suggest one? - i have chosen the graph representation

We would like to see basic metrics related to our site, namely:

* What is the distance between the most distant subpages? - done in graph analysis
* What is the average number of links coming out of subpages of our website to different websites? - done in graph analysis
* What is the average number of internal links on our website? - done in graph analysis
* Average size of the page in our website (HTML only!). - held as node property, done in graph analysis


* We would like to know if there are any dead (pointing to the non-existing pages) links on our site. - i haven't found any, but given the time that i spent on the task, I wouldn't be surprised if I missed something.
* We are interested in getting the full path printed on the screen as well as its length. - not sure what what it means, nodes are holding full urls of subpages.
* We would like to find the pages which are the most difficult to enter (that have the minimal number of incoming links) when using our webpage. - done in graph analysis
* We would also like to find pages which are most linked in our website.

Consider different approaches (web technologies, map representations etc.) and pick the best one in your opinion.

Nice to have (optional, not required):

* We would like to have a text-based UI which allows to ask about the shortest path between two provided subpages (e.g. between https://globalapptesting.com and https://www.globalapptesting.com/customers/facebook) - not really UI, but there is a function with examples of finding shortest paths between two nodes.
* We would like to see graphical representation of the subpages of our website and their relations. - basic png plot generated in globaltesting_task.py
* We would like to have the ability to save the generated map and restore it from the file to avoid the need of regenerating the whole map. - done