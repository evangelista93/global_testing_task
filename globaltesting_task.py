import requests
import re
import networkx as nx
import matplotlib.pyplot as plt


def is_href_internal(href):
    check1 = True if 'https://www.globalapptesting.com' in href else False
    check2 = True if 'https://go.globalapptesting' in href else False
    return check1 or check2


def get_all_links(hlink):
    r = requests.get(hlink)
    regex = "href=\"(.*?)\""
    all_hrefs = set(re.findall(regex, r.text))

    links_connections = []
    for href in all_hrefs:
        if any(ext in href for ext in ['.png', '.css', '.js', '.jpeg', '.jpg', '.xml']): continue
        if href.startswith('//'): href = 'https:' + href # for the ones starting with //go.
        if href.startswith('/'): href = 'https://www.globalapptesting.com' + href # for the ones written like '/security/facebook'
        if not '/' in href: continue # not a link

        internal = is_href_internal(href)
        href_data = {
                    'internal': internal, 
                    'color': 'Green' if internal else 'Blue', 
                    'length_of_html': len(r.content)
                    }
        links_connections.append((href, href_data))

    return links_connections


def website_search(hlink, G, visited):
    nodes = get_all_links(hlink[0])
    G.add_nodes_from(nodes)
    G.add_edges_from([(hlink[0], node[0]) for node in nodes])

    visited.add(hlink[0])
    for node in nodes:  
        if node[0] not in visited and node[1]['internal']:
            website_search(node, G, visited)
    return G


def create_website_map(website_url):
    visited = set()
    G = nx.Graph()
    website_map = website_search(website_url, G, visited)
    return website_map


def main():
    # creating website map
    website_map = create_website_map(['https://www.globalapptesting.com', {'internal':True, 'color': 'Green'}])

    # save map to a file
    nx.write_gpickle(website_map, "website_map.gpickle")

    # draw graph
    cmap = [c[1]['color'] for c in website_map.nodes(data=True)]
    nx.draw(website_map, node_color=cmap, with_labels=False)
    plt.savefig("graph.png")


if __name__ == '__main__':
    main()

